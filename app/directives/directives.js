'use strict';

app.directive('treeSidebar', [function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $.AdminLTE.tree('.sidebar');
        }
    };
}]);