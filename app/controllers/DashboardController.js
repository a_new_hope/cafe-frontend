'use strict';

app.controller('DashboardCtrl', ['$scope', '$window', '$location', 'LoginService', 'NotificationService', function ($scope, $window, $location, LoginService, NotificationService) {

    $scope.logout = function () {
        LoginService.logout().then(
            function () {
                $location.path('/');
            },
            function (error) {
                $scope.error = error;
            }
        );
    };

    $scope.pop = function () {
        NotificationService.success('yeeee');
    };

}]);

app.controller('AllOrdersController', ['$scope', 'NotificationService', function ($scope, NotificationService) {
    console.log('all orders page')
}]);

app.controller('OrderCreateController', ['$scope', 'NotificationService', 'CategoryMenuRestService', 'ProductRestService', 'TableRestService', 'OrderRestService', function ($scope, NotificationService, CategoryMenuRestService, ProductRestService, TableRestService, OrderRestService) {
    $scope.categories = CategoryMenuRestService.get({parent: false}); // initial query
    $scope.products = {};
    $scope.prevState = []; // очередь, в которой сохраняются id категорий, в которые мы входим
    $scope.order = { // таблица заказа, содержащая список заказанных продуктов
        items: []
    };
    $scope.tables = TableRestService.get({status: 0}); // получить только свободные столы

    $scope.viewCategory = function (child) {
        $scope.prevState.push(child.parent); // добавить id текущий категории в очередь
        CategoryMenuRestService.get({parent: child.id}).$promise.then(function(category) {
            if (category.count){
                $scope.categories = category;
            } else {
                $scope.products = ProductRestService.get({category: child.id});
                $scope.categories = {};
            }
        });
    };

    $scope.back = function(){
        // получить последний id очереди. данный id будет являться предыдущей категорией, которую мы посещали
        var prev = $scope.prevState[$scope.prevState.length - 1];
        if (prev){ // если id есть, то загружаем предыдущую категорию
            $scope.categories = CategoryMenuRestService.get({parent: prev});
        } else { // если id отсутствует значит предыдущая категория была родительской, загружаем категории, у которых parent=Null
            $scope.categories = CategoryMenuRestService.get({parent: false});
        }
        // очищаем очередь: удаляем последний элемент массива, который является предыдущей категорией
        $scope.prevState.pop();
        // когда мы возвращаемся на предыдущий уровень, данный уровень всегда является категорией продуктов,
        // поэтому очищаем продукты и оставляем только категории
        $scope.products = {}
    };

    $scope.addToOrder = function(product){
        var isExist = false;
        if (typeof(product.quantity) === 'undefined'){
            product.quantity = 1; // у продукта нет свойства "количество". во время добавления продукта в заказ, добавляем новое свойство
        }
        angular.forEach($scope.order.items, function(value, key) {
            if (value.id == product.id){ // если данный продукт уже имеется в заказе, то увеличиваем quantity на 1
                $scope.order.items[key].quantity += 1;
                isExist = true;
            }
        });

        if (isExist){ // не нужно добавлять продукт, который уже добавлен в заказ
            return
        }
        $scope.order.items.push(product); // если продукт не был ранее добавлен, добавляем в заказ
    };

    $scope.removeFromOrder = function(index){
        $scope.order.items.splice(index, 1);
    };

    $scope.saveOrder = function(){
        if (!$scope.order.table){
            NotificationService.error('Choose a table');
            return;
        }
        else if ($scope.order.items.length === 0){
            NotificationService.error('You can`t save empty order' );
            return;
        }
        $scope.newOrder = new OrderRestService();
        $scope.newOrder.table = $scope.order.table.id;
        $scope.newOrder.products = $scope.order.items;
        $scope.newOrder.amount = '10.00';
        $scope.newOrder.$save(function(){
            console.log('saving...')
        });
    };
}]);
