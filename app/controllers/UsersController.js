'use strict';

app.controller('UsersController', ['$scope', '$location', 'UserRestService', function ($scope, $location, UserRestService) {

    $scope.users = UserRestService.get();

    $scope.deleteUser = function (userId, index) {
        UserRestService.delete({ id: userId }, function(){
            $scope.users.splice(index, 1);
        });
    };

    console.log('2222222222');

}]);


app.controller('UsersCreateController', ['$scope', '$location', 'UserRestService', '$state', function ($scope, $location, UserRestService, $state) {

    $scope.user = new UserRestService();
    $scope.user.stuffuser = {};

    $scope.genderOptions = [
	    { label: 'Male', value: 0 },
	    { label: 'Female', value: 1 }
	];

	$scope.roleOptions = [
	    { label: 'Administrator', value: 0 },
	    { label: 'Manager', value: 1 },
	    { label: 'Worker', value: 2 }
	];

	$scope.user.stuffuser.gender = 0;
	$scope.user.stuffuser.role = 2;

    $scope.createUser = function(){
    	$scope.user.stuffuser = {
    		gender: $scope.user.stuffuser.gender,
    		role: $scope.user.stuffuser.role
    	};
        $scope.user.$save(function(){
            $state.go('management.users.list');
        });
    };

}]);


app.controller('UsersUpdateController', ['$scope', '$location', 'UserRestService', '$stateParams', function ($scope, $location, UserRestService, $stateParams) {

	$scope.genderOptions = [
	    { label: 'Male', value: 0 },
	    { label: 'Female', value: 1 }
	];

	$scope.roleOptions = [
	    { label: 'Administrator', value: 0 },
	    { label: 'Manager', value: 1 },
	    { label: 'Worker', value: 2 }
	];

	$scope.loadUser = function(){
		UserRestService.get({id:$stateParams.id},function(user){
		    $scope.user = user;
		});
	};

	$scope.loadUser();

	$scope.updateUser = function(){
        $scope.user.$update({ id:$scope.user.id }, function(){
            console.log('user updated');
        });
    };

}]);

app.controller('UsersViewController', ['$scope', '$location', 'UserRestService', '$stateParams', function ($scope, $location, UserRestService, $stateParams) {

	$scope.genderOptions = [
	    { label: 'Male', value: 0 },
	    { label: 'Female', value: 1 }
	];
    console.log('111111111111111111');
	$scope.roleOptions = [
	    { label: 'Administrator', value: 0 },
	    { label: 'Manager', value: 1 },
	    { label: 'Worker', value: 2 }
	];

	$scope.loadUser = function(){
		UserRestService.get({id:$stateParams.id},function(user){
		    $scope.user = user;
		    console.log(user);
		    $scope.user.stuffuser.gender = $scope.genderOptions[user.stuffuser.gender];
			$scope.user.stuffuser.role = $scope.roleOptions[user.stuffuser.role];

		});
	};

	$scope.loadUser();

}]);