'use strict';

app.controller('CategoryMenuListController', ['$scope', 'CategoryMenuRestService', function ($scope, CategoryMenuRestService) {
    $scope.categories = CategoryMenuRestService.get();

    $scope.deleteUser = function (catId, index) {
        CategoryMenuRestService.delete({id: catId}, function () {
            $scope.categories.splice(index, 1);
        });
    };
}]);

