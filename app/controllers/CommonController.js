'use strict';

app.controller('MasterController', ['$scope', '$window', '$location', 'UserService', function ($scope, $window, $location, UserService) {

    // watches for changes in a value from the service and then syncs the changed values to the scope
    $scope.$watch(UserService.isLogged, function (isLogged) {
        $scope.isLogged = isLogged;
        $scope.currentUser = UserService.currentUser();
    });

    $scope.showSidebar = function(){
        //If window is small enough, enable sidebar push menu
        if ($(window).width() <= 992) {
            $('.row-offcanvas').toggleClass('active');
            $('.left-side').removeClass("collapse-left");
            $(".right-side").removeClass("strech");
            $('.row-offcanvas').toggleClass("relative");
        } else {
            //Else, enable content streching
            $('.left-side').toggleClass("collapse-left");
            $(".right-side").toggleClass("strech");
        }
    };

}]);

app.controller('GlobalNoticeController', ['$scope', 'GlobalNoticeService', function ($scope, GlobalNoticeService) {
    $scope.messages = GlobalNoticeService.messages;
}]);