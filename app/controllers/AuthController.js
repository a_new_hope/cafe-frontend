'use strict';

app.controller('AuthCtrl', ['$scope', '$location', 'LoginService', function ($scope, $location, LoginService) {
    $scope.register = function () {
        var username = $scope.registerUsername;
        var password = $scope.registerPassword;

        if (username && password) {
            LoginService.register(username, password).then(
                function () {
                    $location.path('/dashboard');
                },
                function (error) {
                    $scope.registerError = error;
                }
            );
        }
        else {
            $scope.registerError = 'Username and password required';
        }
    };

    $scope.login = function () {
        var username = $scope.loginUsername;
        var password = $scope.loginPassword;

        if (username && password) {
            LoginService.login(username, password).then(
                function () {
                  $location.path('/dashboard');
                },
                function (error) {
                  $scope.loginError = error;
                }
            );
        } else {
            $scope.loginError = 'Username and password required';
        }
    };

}]);
