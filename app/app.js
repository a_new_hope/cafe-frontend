'use strict';
/* global app: true */

var app = angular.module('cafeApp', [
    'ui.router',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'toaster',
    'ui.bootstrap'
]);

app.run(['$rootScope', '$window', '$location', 'CheckAuthPermissionsService', function ($rootScope, $window, $location, CheckAuthPermissionsService) {

    //$rootScope.$on('$routeChangeStart', function(event, next, current){
    //    CheckAuthPermissionsService.checkAccess(event, next, current);
    //});

}]);

app.config(['$stateProvider', '$httpProvider', '$urlRouterProvider', 'USER_ROLES', function ($stateProvider, $httpProvider, $urlRouterProvider, USER_ROLES) {
    $httpProvider.interceptors.push('AuthInterceptor');
    $urlRouterProvider.when('', '/all-orders');
    $urlRouterProvider.otherwise('/all-orders');
    $stateProvider
        .state('dashboard', {
            url: "/",
            abstract: true,
            templateUrl: "templates/pages/dashboard/index.html"
        })
        .state('dashboard.all-orders', {
            url: "all-orders",
            templateUrl: "templates/pages/dashboard/all-orders.html",
            controller: 'AllOrdersController'
        })
        .state('dashboard.create-order', {
            url: "create-order",
            templateUrl: "templates/pages/dashboard/create-order.html",
            controller: 'OrderCreateController'
        })
        .state('dashboard.view-order', {
            url: "order/:id",
            templateUrl: "templates/pages/dashboard/create-order.html",
            controller: 'OrderCreateController'
        })

        //.state('menu', { // need remove
        //    url: "/menu",
        //    templateUrl: "templates/pages/menu/index.html",
        //    controller: 'MenuController',
        //    permissions: {
        //        roles: [USER_ROLES.authenticated]
        //    }
        //})

        .state('management', {
            url: "/management",
            abstract: true,
            template: '<ui-view/>',
            //permissions: {
            //    roles: [USER_ROLES.administrator, USER_ROLES.manager]
            //}
        })
        .state('management.users', {
            url: "/users",
            abstract: true,
            templateUrl: "templates/pages/users/index.html",
            //permissions: {
            //    roles: [USER_ROLES.administrator, USER_ROLES.manager]
            //}
        })
        .state('management.users.list', {
            url: "/list",
            templateUrl: "templates/pages/users/list.html",
            controller: 'UsersController',
            //permissions: {
            //    roles: [USER_ROLES.administrator, USER_ROLES.manager]
            //}
        })
        .state('management.users.view', {
            url: "/:id/view",
            templateUrl: "templates/pages/users/view.html",
            controller: 'UsersViewController',
            //permissions: {
            //    roles: [USER_ROLES.administrator, USER_ROLES.manager]
            //}
        })
        .state('management.users.create', {
            url: "/create",
            templateUrl: "templates/pages/users/create.html",
            controller: 'UsersCreateController',
            permissions: {
                roles: [USER_ROLES.administrator, USER_ROLES.manager]
            }
        })
        .state('management.users.update', {
            url: "/:id/update",
            templateUrl: "templates/pages/users/update.html",
            controller: 'UsersUpdateController',
            permissions: {
                roles: [USER_ROLES.administrator, USER_ROLES.manager]
            }
        })

        .state('login', {
            url: "/login",
            templateUrl: "templates/pages/login.html",
            controller: 'AuthCtrl',
            permissions: {
                roles: false
            }
        })
}]);

app.constant('API_SERVER', 'http://127.0.0.1:8000/api/');
app.constant('USER_ROLES', {administrator: 0, manager: 1, worker: 2, authenticated: 3});
