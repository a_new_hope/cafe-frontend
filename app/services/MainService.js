'use strict';

app.factory('NotificationService', ['toaster', function (toaster) {
    return {
        success: function (text) {
            toaster.pop('success', 'Success notification', text);
        },
        error: function (text) {
            toaster.pop('error', "Error notification", text);
        },
        warning: function (text) {
            toaster.pop('warning', "Warning notification", text);
        },
        note: function (text) {
            toaster.pop('note', "Warning notification", text);
        }
    };
}]);

app.factory('StorageService', [function() {
    var service = {
        getItem: function(key) {
            var val = localStorage.getItem(key);
            if(val) {
                return JSON.parse(val);
            }
            else{
                return false;
            }
        },
        setItem: function(key,obj) {
            localStorage.setItem(key, JSON.stringify(obj));
        },
        removeItem: function(key) {

        },
        clear: function(){
            localStorage.clear();
        },
    };
    return service;
}]);

/**
 *  Используется глобально для всего app. Реагирует на все запросы в AuthInterceptor
 */
app.factory('GlobalNoticeService', [ function() {

    var messages =  {
        errors: [],
        success: [],

        clear:function(){
            this.errors= [];
            this.success= [];
        }
    };

    var srv = {
        messages: messages,
        addError:function(error) {
            this.clear();
            this.messages.errors.push(error);
        },

        addSuccess:function(message) {
            this.clear();
            this.messages.success.push(message);
        },
        clear:function() {
            this.messages.clear();
        }
    };

    return srv;
}]);