'use strict';

app.factory('LoginService', ['$http', '$window', '$q', 'API_SERVER', 'StorageService', function ($http, $window, $q, API_SERVER, StorageService) {

    var authenticate = function (username, password, endpoint) {
        var url = API_SERVER + endpoint;
        var data;
        var deferred = $q.defer();

        data = {
            'username': username,
            'password': password
        };

        $http.post(url, data).then(
            function (response) {
                var token = response.data.token;
                var user = response.data.user;

                if (token && username) {
                    $window.localStorage.token = token;
                    // StorageService.setItem("token", token)
                    StorageService.setItem("user", user);
                    deferred.resolve(true);
                }
                else {
                    deferred.reject('Invalid data received from server');
                }
            },
            function (response) {
                deferred.reject(response.data.error);
            }
        );
    return deferred.promise;
    };

    var logout = function () {
        var deferred = $q.defer();
        var url = API_SERVER + 'logout/';

        $http.post(url).then(
            function () {
                $window.localStorage.removeItem('token');
                $window.localStorage.removeItem('username');
                deferred.resolve();
            },
            function (error) {
                deferred.reject(error.data.error);
            }
        );
        return deferred.promise;
    };

    return {
        register: function (username, password) {
            return authenticate(username, password, 'register/');
        },
        login: function (username, password) {
            return authenticate(username, password, 'login/');
        },
        logout: function () {
            return logout();
        }
    };

}]);

app.factory('UserService', ['$window', 'StorageService', '$location', function ($window, StorageService, $location) {
    var service = {
        init: function(){
            service.postActions();
        },
        postActions: function(){
            console.log('post actions');
        },
        isLogged: function(){
            return ($window.localStorage.token);
        },
        currentUser: function(){
            return StorageService.getItem("user");
        },
        isInRole: function(roles){
            return roles.indexOf(service.currentUser().role) > -1;
        },
        logout: function(){
            $window.localStorage.removeItem('token');
            $window.localStorage.removeItem('username');
            $location.path('/');
        },
        isAdministrator: function() {
            return service.currentUser().role === 1; // 1 - administrator
        }
    };
    // service.init();
    return service;
}]);

app.factory('CheckAuthPermissionsService', ['UserService', '$location', 'USER_ROLES', function (UserService, $location, USER_ROLES) {
    return {
        checkAccess: function (event, next, current) {
            var roles = next.permissions.roles;

            if(roles && UserService.isLogged()){

                if (roles.indexOf(USER_ROLES.authenticated) && !UserService.isInRole(roles)){
                    alert('access denied');
                    event.preventDefault();
                }

            } else if (!roles && UserService.isLogged()){
                // например, страница /login
                $location.path('/');

            } else if (roles && !UserService.isLogged()) {

                $location.path('/login');

            } else {

                $location.path('/login');

            }
        }
    };
}]);