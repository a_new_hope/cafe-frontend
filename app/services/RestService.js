'use strict';

app.factory('UserRestService', ['$resource', 'API_SERVER', function ($resource, API_SERVER) {
    return $resource(API_SERVER + 'users/:id/?format=json', {id:'@_id'},
    {
    	isArray : false,
        update: {
            method: 'PUT'
        }
    });
}]);

app.factory('CategoryMenuRestService', ['$resource', 'API_SERVER', function ($resource, API_SERVER) {
    return $resource(API_SERVER + 'category-menu/:id/?format=json', {id:'@_id'},
    {
    	isArray : false,
        update: {
            method: 'PUT'
        }
    });
}]);

app.factory('ProductRestService', ['$resource', 'API_SERVER', function ($resource, API_SERVER) {
    return $resource(API_SERVER + 'products/:id/?format=json', {id:'@_id'},
    {
    	isArray : false,
        update: {
            method: 'PUT'
        }
    });
}]);

app.factory('TableRestService', ['$resource', 'API_SERVER', function ($resource, API_SERVER) {
    return $resource(API_SERVER + 'tables/:id/?format=json', {id:'@_id'},
    {
    	isArray : false,
        update: {
            method: 'PUT'
        }
    });
}]);

app.factory('OrderRestService', ['$resource', 'API_SERVER', function ($resource, API_SERVER) {
    return $resource(API_SERVER + 'orders/:id/?format=json', {id:'@_id'},
    {
    	isArray : false,
        update: {
            method: 'PUT'
        }
    });
}]);
