'use strict';

app.factory('AuthInterceptor', ['$rootScope', '$q', '$window', '$location', 'GlobalNoticeService', 'UserService', function ($rootScope, $q, $window, $location, GlobalNoticeService, UserService) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.token) {
                config.headers.Authorization = 'Token ' + $window.localStorage.token;
            }
            return config;
        },

        responseError: function (response) {
            switch (response.status) {
                case 400: // Bad Request
                    GlobalNoticeService.addError(response.data);
                    break;
                case 401: // Unauthorized
                    UserService.logout();
                    break;
            }
            return $q.reject(response);
        }
    };
}]);
