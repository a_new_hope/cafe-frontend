# Установка проекта #

Установка необходимых пакетов

```
#!bash

curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs build-essential
sudo npm install -g grunt-cli bower
npm install
bower install
```
grunt {dev:prod} - для сборки проекта под определенное окружение

grunt watch {path/to/watchable/js_css} отслеживает измининие js и css скриптов и минифицирает их на лету. В зависимости от окружения скрипты будут либо min (prod)  либо исходные (dev). Папку указывать не обязательно.

Для сборки проекта в development окружении.
```
#!bash
grunt dev && grunt watch
```

Запуск node server
```
#!bash

node server.js
```
После приложение будет доступно по адресу http://localhost:2015/


Настройка nginx сервера
```
#!bash

sudo touch /etc/nginx/sites-available/cafe.ls
sudo ln -s /etc/nginx/sites-available/cafe.ls /etc/nginx/sites-enabled/
sudo nano /etc/nginx/sites-available/cafe.ls
```

Далее вставляем в /etc/nginx/sites-available/cafe.ls этот конфиг


```
#!bash

server {
    server_name cafe.ls www.cafe.ls;

    access_log   /var/www/cafe-frontend/logs/access.log;
    error_log    /var/www/cafe-frontend/logs/error.log;

    root /var/www/cafe-frontend;
    index index.html;
    charset utf-8;

    location / {
        try_files $uri $uri/ /index.html =404;
  }
}
```
Добавляем в /etc/hosts новую запись 
```
#!bash

127.0.1.1       cafe.ls
```

Перезапускаем nginx
```
#!bash

sudo service nginx restart
```

Приложение будет доступно по адресу http://cafe.ls/#/

## Автоматическое разворачивание проекта ##
```
#!bash

sh configs/install.sh
```
## Обновление проекта ##
Чтобы обновить проект (backend, frontend, database) выполните команду:
```
#!bash

sh configs/update.sh
```
