/**
 * https://github.com/gruntjs/grunt-contrib-cssmin
 */
module.exports = {
    all: {
        files: [{
            expand: true,
            cwd: 'static/css',
            src: ['*.css'],
            dest: 'assets/css',
            ext: '.min.css'
        }]
    }
};