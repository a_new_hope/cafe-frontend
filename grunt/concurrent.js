/**
 * https://github.com/sindresorhus/grunt-concurrent
 */

module.exports = {

    options: {
        limit: 3
    },

    devFirst: [
        'clean',
        'jshint'
    ],
    devSecond: [
        'uglify:dev',
        'cssmin'
    ],

    prodFirst: [
        'clean',
        'jshint'
    ],
    prodSecond: [
        'uglify:prod',
        'cssmin'
    ],

    imgFirst: [
        'imagemin'
    ]
};