/**
 * https://github.com/gruntjs/grunt-contrib-imagemin
 */
module.exports = {
    all: {
        files: [{
            expand: true,
            cwd: 'static/',
            src: ['images/*.{png,jpg,gif}'],
            dest: 'assets/'
        }]
    }
};