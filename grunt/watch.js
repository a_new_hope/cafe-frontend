/**
 * https://github.com/gruntjs/grunt-contrib-watch
 */
module.exports = {
    options: {
        spawn: false,
        livereload: true
    },
    scripts: {
        files:[
            'app/**/*.js',
            'static/js/*.js'
        ],
        tasks: [
            'jshint',
            'uglify'
        ]
    },
    styles: {
        files: [
            'static/css/*.css'
        ],
        tasks: [
            'cssmin'
        ]
    }
};