/**
 * https://github.com/gruntjs/grunt-contrib-uglify
 */
module.exports = {
    dev: {
        options: {
            mangle: false,
            compress: false,
            preserveComments: 'all',
            beautify: true
        },
        files: [{
            expand: true,
            cwd: 'app/',
            src: '**/*.js',
            dest: 'assets/app',
            ext: '.min.js'
        },{
            expand: true,
            cwd: 'static/js/',
            src: '**/*.js',
            dest: 'assets/js',
            ext: '.min.js'
        }]
    },
    prod: {
        options: {
            mangle: true,
            compress: true
        },
        files: [{
            expand: true,
            cwd: 'app/',
            src: '**/*.js',
            dest: 'assets/app',
            ext: '.min.js'
        },{
            expand: true,
            cwd: 'static/js/',
            src: '**/*.js',
            dest: 'assets/js',
            ext: '.min.js'
        }]
    }
};