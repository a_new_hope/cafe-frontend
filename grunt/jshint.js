/**
 * https://github.com/gruntjs/grunt-contrib-jshint
 * https://github.com/jshint/jshint/blob/master/examples/.jshintrc
 */
module.exports = {
    options: {
        reporter: require('jshint-stylish'),
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        eqnull: true,
        browser: true,
        nomen: false,
        strict: true,
        globalstrict: true,
        globals: {
            console: true,
            require: true,
            define: true,
            _: true,
            $: true,
            app: true,
            angular: true,
            alert: true,
            jQuery: true
        }
    },
    main: [
        'app/**/*.js',
        'static/js/**/*.js'
    ]
};