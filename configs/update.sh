#!/bin/sh

echo "============================="
echo "======Updating frontend======"
echo "============================="

git pull
bower install

echo "============================="
echo "======Updating backend======="
echo "============================="

cd ../cafe-backend
git pull

echo "Updating requirements"
.env/bin/pip install -r requirements.txt

echo "Updating database"
sudo -u postgres psql -c "DROP DATABASE cafe_dev;"
sudo -u postgres psql -c "CREATE DATABASE cafe_dev;"
sudo -u postgres psql cafe_dev < cafe_dev.sql
