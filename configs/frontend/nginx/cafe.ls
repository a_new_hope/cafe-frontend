server {
    server_name cafe.ls www.cafe.ls;

    access_log   /var/www/cafe-frontend/logs/access.log;
    error_log    /var/www/cafe-frontend/logs/error.log;

    root /var/www/cafe-frontend;
    index index.html;
    charset utf-8;

    location / {
        try_files $uri $uri/ /index.html =404;
  }
}