#!/bin/sh

PYTHON=python2.7

echo "Checking git"

if which git > /dev/null
	then echo "git - OK"
else
	echo "No git. Installing git."
	sudo apt-get install git-core
fi

echo "============================="
echo "=====Installing frontend====="
echo "============================="

git clone git@bitbucket.org:a_new_hope/cafe-frontend.git
cd cafe-frontend

echo "Installing node.js"
curl -sL https://deb.nodesource.com/setup | sudo bash -
sudo apt-get install -y nodejs build-essential
sudo npm install -g grunt-cli bower
npm install
bower install

echo "Nginx configuration for cafe.ls"
sudo cp configs/frontend/nginx/cafe.ls /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/cafe.ls /etc/nginx/sites-enabled/

echo "Adding new hostname to /etc/hosts"
echo "127.0.1.1   cafe.ls" >> /etc/hosts

echo "Restarting nginx"
sudo service nginx restart



echo "============================="
echo "=====Installing backend====="
echo "============================="

cd ../

echo "Installing necessary packages"
sudo apt-get install python3 libpq-dev python-dev postgresql postgresql-contrib

echo "Creating database"
sudo -u postgres psql -c "CREATE DATABASE cafe_dev;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE cafe_dev TO postgres;"
sudo service postgresql restart

echo "Installing pip and virtualenv"
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install virtualenv

echo "Cloning the project"
git clone git@bitbucket.org:a_new_hope/cafe-backend.git
cd cafe-backend
virtualenv .env
.env/bin/pip install -r requirements.txt
.env/bin/python manage.py migrate
.env/bin/python manage.py createsuperuser
.env/bin/python manage.py runserver 0.0.0.0:8000
